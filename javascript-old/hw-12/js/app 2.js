const imgs = document.querySelectorAll('.image-to-show');
const btnStop = document.createElement('button');
const btnProceed = document.createElement('button');
const timer = document.createElement("p");
let time = 10;
let i = 1;

btnStop.textContent = 'Прекратить';
document.body.append(btnStop);
btnProceed.textContent = 'Возобновить показ';
document.body.append(btnProceed);
btnProceed.disabled = true;

document.body.prepend(timer);
timer.textContent = time;

const showImg = function  () {
    imgs.forEach(element =>{
        if(element.classList.contains('show')){
            element.classList.remove('show');
        }
    });
    imgs[i].classList.add('show');

    i++;
    if (i === imgs.length){
        i=0;
    }
};

const showTimer = function (){
    if (time === 1){
        time = 10;
        showImg();
    } else {
        time--;
    }
    timer.textContent = time;
};

let timeShow = setInterval(showTimer, 1000);

btnStop.addEventListener('click', event => {
    clearInterval(timeShow);
    btnProceed.disabled = false;
});

btnProceed.addEventListener('click', () =>{
    timeShow = setInterval(showTimer, 1000);
    btnProceed.disabled = true;
});
