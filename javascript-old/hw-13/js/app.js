const button = document.querySelector('.subscribe-now-button');
const header = document.head;

if(localStorage.getItem('css' , 'css/style-active.css') !==null) {
    header.querySelector('link[href="css/style.css"]').href = 'css/style-active.css';
}

button.addEventListener('click', event => {
    if(header.querySelector('link[href="css/style.css"]')){
        header.querySelector('link[href="css/style.css"]').href = 'css/style-active.css';
        localStorage.setItem('css' , 'css/styleActive.css');
    } else {
        header.querySelector('link[href="css/style-active.css"]').href = 'css/style.css';
        localStorage.clear();
    }
});