const tab = document.querySelectorAll('.tabs-title');
const tabItem = document.querySelectorAll('.tabs-item');
const tabWrap = document.querySelector('.tabs');

tabWrap.addEventListener('click', event => {
    const tabActive = document.querySelector(`[data-tab-content ='${event.target.dataset.tabTitle}']`);
    tabItem.forEach(element => element.classList.remove('tabs-active'));
    tab.forEach(element => element.classList.remove('active'));
    event.target.classList.add('active');
    tabActive.classList.add('tabs-active');
});