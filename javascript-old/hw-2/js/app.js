let number = +prompt("Enter the number");

if(number >= 5){
    for (let i = 0; i <= number; i++) {
        if (i % 5 === 0){
            console.log(i);
        }
    }
} else if (number <= -5) {
    for (let i = 0; i >= number; i--) {
        if (i % 5 === 0) {
            console.log(i);
        }
    }
} else {
    console.log("Sorry, no numbers");
}