let name = prompt(`Hello! Write your name.`);
let age = +prompt(`Write your age.`);
let adult = `Welcome, ${name}.`;
let accessBan = (`You are not allowed to visit this website`);

if (age > 0) {
	if(age < 18) {
		alert(accessBan);
	} else if(age >= 18 && age < 22) {
		let pass = confirm(`Are you sure you want to continue?`);
		if (pass === true) {
			alert(adult);
		} else {
			alert(accessBan);
		}
	} else {
		alert(adult);
	}
} else {
	alert(`Age is incorrect`);
}