const wrap = document.getElementById('wrap');
const inputPrice = document.getElementById("input-price");
wrap.style.display = "block";
const span = document.createElement('span');
const incorrect = document.createElement('span');

inputPrice.addEventListener("focus", (evt) => {
    evt.target.style.cssText = "border: 3px solid #0f0";
});

inputPrice.addEventListener("blur", () => {
    let val = +inputPrice.value;
    if (val < 0 ){
        incorrect.textContent = "Please enter correct price";
        wrap.after(incorrect);
        return;
    }
    if (incorrect){
        incorrect.remove();
    }
    if(span){
        val = +inputPrice.value;
    }
    span.textContent = `Текущая цена: ${val}`;
    const deleteVal = document.createElement('span');
    deleteVal.textContent = 'x';
    wrap.before(span);
    span.prepend(deleteVal);
    inputPrice.style.color = "green";

    deleteVal.addEventListener('click', ()=> {
        inputPrice.value = "";
        span.remove();
    });
});