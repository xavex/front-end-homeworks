const calcNumber = function (numOne, numTwo, operation) {
    switch (operation) {
        case "+":
            return numOne + numTwo;
        case "-":
            return numOne - numTwo;
        case "*":
            return numOne * numTwo;
        case  "/":
            return  numOne / numTwo;
    }
};
let firstNumber = +prompt('Enter the first number');
let secondNumber = +prompt('Enter the second number');
let operation = prompt('Enter operation');

console.log(calcNumber(firstNumber, secondNumber, operation));