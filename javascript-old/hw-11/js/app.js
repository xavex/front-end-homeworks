const button = document.querySelectorAll('.button');

document.addEventListener('keydown', (event) => {
    button.forEach(elem => {
        let styleElem = getComputedStyle(elem);
        if (styleElem.backgroundColor === 'rgb(0, 0, 255)') {
            elem.style.backgroundColor = 'black';
        }
        if (elem.textContent.toLowerCase() === event.key.toLowerCase()) {
            elem.style.backgroundColor = 'blue';
        }
    });
});