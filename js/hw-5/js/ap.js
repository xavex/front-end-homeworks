function createNewUser() {

    const newUser = {}
    const newFirstName = prompt("Enter the first name: ");
    const newLastName = prompt("Enter the last name: ");
    const newBirthday = prompt("Enter the birth date (dd.mm.yyyy): ");

    newUser.firstName = newFirstName;
    newUser.lastName = newLastName;
    newUser.birthday = newBirthday;
    newUser.getLogin = function()
    {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    };
    newUser.getAge = function() {
        if (new Date().getMonth() + 1 - +this.birthday.slice(3, 5) > 0 || (new Date().getMonth() + 1 - +this.birthday.slice(3, 5) === 0 && (new Date().getDate() - +this.birthday.slice(0, 2) >= 0))) {
            return [new Date().getFullYear()] - +this.birthday.slice(-4)
        } else { return [new Date().getFullYear()] - +this.birthday.slice(-4) - 1 }
    };
    newUser.getPassword = function() {
        return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + +this.birthday.slice(-4)
    }
    return newUser;
}

const user = createNewUser();
console.log(user);

console.log(`The proposed login: ${user.getLogin()}, 
User age: ${user.getAge()} , 
The proposed password: ${user.getPassword()}`);
