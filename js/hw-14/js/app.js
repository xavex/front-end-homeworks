$(".navbar a[href]").on("click", function(){
    $('html').animate({
        scrollTop: $($(this).attr('href')).offset().top-50
    }, 1000);
});

let height = document.documentElement.clientHeight;

$(window).on('scroll', function () {
    if ($(window).scrollTop() > height){
        $('.button-to-top').addClass('button-to-top-active ');
    } else {
        $('.button-to-top').removeClass('button-to-top-active');
    }
});

$('.button-to-top').on('click', function () {
    $('html').animate({scrollTop:0}, 500)
});

$('.posts-button').on('click', function () {
    $('.card-wrap').slideToggle('slow');
});
